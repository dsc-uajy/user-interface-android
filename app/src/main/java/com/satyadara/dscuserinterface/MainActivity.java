package com.satyadara.dscuserinterface;

import android.app.ActivityOptions;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    /**FIELD**/
    private TextView mTitle;
    private EditText mDefaultEd;
    private EditText mMaterialEd;
    private Button mSubmitButton;
    private Button mClearButton;
    private RadioGroup mRadioGrup;
    private RadioButton mOptionA;
    private RadioButton mOptionB;
    private Spinner mSpinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Lakukan yang diperlukan saat activity terbentuk.
        initialize();
    }

    private void initialize() {
        /**
         * Menghubungkan field pada MainActivity.java dengan tampilan
         * dengan mengambil id pada tampilan dan di casting
         * berdasarkan field
         */
        mTitle = (TextView) findViewById(R.id.title);
        mDefaultEd = (EditText) findViewById(R.id.edDefault);
        mMaterialEd = (EditText) findViewById(R.id.edMaterial);
        mSubmitButton = (Button) findViewById(R.id.buttonSubmit);
        mClearButton = (Button) findViewById(R.id.buttonClear);
        mRadioGrup = (RadioGroup) findViewById(R.id.radioGroup);
        mOptionA = (RadioButton) findViewById(R.id.radioBtnA);
        mOptionB = (RadioButton) findViewById(R.id.radioBtnB);
        mSpinner = (Spinner) findViewById(R.id.spinner);
    }

    public void onClearClicked(View view) {
        /**
         * Membersihkan form kembali menjadi kosong / dalam keadaan semula
         */
        mDefaultEd.setText("");
        mMaterialEd.setText("");
        mOptionA.setChecked(false);
        mOptionB.setChecked(false);
        mSpinner.setSelection(0);
    }

    public void onSubmitClicked(View view) {
        Intent intent = new Intent(this, ResultActivity.class);
        startActivity(intent);
    }
}

